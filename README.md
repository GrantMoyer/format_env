Substitues environment variables into a string literal at compile time.

Example:

```rust
use format_env::format_env;
assert_eq!(format_env!("Name: $(CARGO_PKG_NAME), License: $(CARGO_PKG_LICENSE)"), "Name: format_env, License: MIT")
assert_eq!(format_env!("Name: $$(CARGO_PKG_NAME)"), "Name: $(CARGO_PKG_NAME)")
```
